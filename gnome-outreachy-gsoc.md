# GSoC y Outreachy en GNOME

Con los programas de becas del google summer of code (GSoC) y el programa
outreachy se busca que nuevos desarrolladores colaboren dentro del proyecto
GNOME durante unos meses dedicados a un proyecto en concreto y con una
retribución.

## Formato de la propuesta

Indicar uno de estos:

* [x] Charla (1 hora)

## Descripción

Breve repaso de las posibles becas (GSoC & Outreachy) que se pueden conseguir
por colaborar con GNOME, así como una pequeña explicación de los pasos a seguir
para poder optar a estas becas.

También daremos un repaso a qué es necesario para ser mentor y las
responsabilidades del mismo.

## Público objetivo

Estudiantes y otras personas que estén interesados en colaborar con GNOME

## Ponente(s)

 * danigm (Mentor en varios Outreachy, GSoC)

### Contacto(s)

* Nombre: Daniel Garcia Moreno
* Contacto: danigm @ gnome.org

## Comentarios

Cualquier otro comentario relevante.

## Condiciones

* [x] Acepto seguir el [código de conducta](https://eslib.re/conducta/) y solicitar a los asistentes y ponentes esta aceptación.
* [x] Al menos una persona entre los que la proponen estará presente el día programado para la charla.
